import Vue from 'vue'
import Router from 'vue-router'

// Static components
import AppAbout from '../components/static/AppAbout'
import NotFound from '../components/static/NotFound'
// Auth components
import Login from '../components/auth/Login'
import Register from '../components/auth/Register'
import Logout from '../components/auth/Logout'
// Profile components
import MyAcount from '../components/profile/MyAcount'
import MyAddresses from '../components/profile/MyAddresses'
import MyOrders from '../components/profile/MyOrders'
import ProfileSettings from '../components/profile/Settings'
// Model components
import Product from '../components/model/Product'
import Order from '../components/model/Order'
import Address from '../components/model/Address'
import AddressForm from '../components/model/AddressForm'
// Other components
import Home from '../components/Home'
import Category from '../components/Category'
// Cart components
import Cart from '../components/shopping/Cart'
import Checkout from '../components/shopping/Checkout'


Vue.use(Router)

export default new Router({

    mode: 'history',
    routes: [{
        path: '/',
        name: 'home',
        component: Home
    }, {
        path: '/about',
        name: 'about',
        component: AppAbout
    }, {
        path: '/login',
        name: 'login',
        component: Login
    }, {
        path: '/register',
        name: 'register',
        component: Register
    }, {
        path: '/logout',
        name: 'logout',
        component: Logout
    }, {
        path: '/category/:id',
        name: 'categoria',
        component: Category,
        props: true
    }, {
        path: '/product/:id',
        name: 'producto',
        component: Product,
        props: true
    }, {
        path: '/order/:id',
        name: 'order',
        component: Order,
        props: true
    }, {
        path: '/order/:id/edit',
        name: 'order-edit',
        component: Order,
        props: true
    }, {
        path: '/address/:id',
        name: 'address',
        component: Address,
        props: true
    }, {
        path: '/address-new',
        name: 'address-new',
        component: AddressForm
    }, {
        path: '/address/edit/:id',
        name: 'address-edit',
        component: AddressForm,
        props: true
    }, {
        path: '/cart',
        name: 'carrito',
        component: Cart,
    }, {
        path: '/checkout',
        name: 'checkout',
        component: Checkout
    }, {
        path: '/my-account',
        name: 'mi-cuenta',
        component: MyAcount
    }, {
        path: '/my-account/orders',
        name: 'mis-pedidos',
        component: MyOrders
    }, {
        path: '/datos',
        name: 'datos',
        component: ProfileSettings
    }, {
        path: '/my-account/settings/edit',
        name: 'editar-ajustes',
        component: ProfileSettings,
        props: true
    }, {
        path: '/my-account/addresses',
        name: 'direcciones',
        component: MyAddresses
    }, {
        path: '/not-found',
        name: '404',
        component: NotFound
    }, {
        path: '*',
        redirect: {
            name: '404'
        }
    }],

})