import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://backoffice.grup01.ddaw.site/api'

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        filter: 'all',
        productos: [],
        categorias: [],
        user: {},
        carrito: [],
        productsLoaded: false
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        },
        carrito(state) {
            return state.carrito;
        },
        loggedUser(state) {
            return state.user;
        },
        productos(state) {
            return state.productos;
        },
        categorias(state) {
            return state.categorias;
        },
        getCategoriaById: (state) => (id) => {
            return state.categorias.find(category => category.id == id);
        },
        getProductoById: (state) => (id) => {
            return state.productos.find(producto => producto.id == id);
        },
        getProductoInCarrito: (state) => (id) => {
            return state.carrito.find(producto => producto.id == id);
        }
    },
    mutations: {
        addProductoToCarrito(state, prodCart) {
            state.carrito.push(prodCart)
        },
        addPedido(state, pedido) {
            state.user.pedidos.push(pedido)
            pedido.lineas.forEach(element => {
                let producto = state.productos.find(producto => producto.id == element.id_producto);
                producto.stock -= element.cantidad
            });
        },
        updateEstado(state, pedido) {
            this.cambio = state.user.pedidos.find(elemento => elemento.id == pedido.id)
            this.cambio.estado.id = 6;
            this.cambio.estado.name = "Cancelado";
        },
        addAddress(state, address) {
            state.user.direcciones.push(address)
        },
        updateAddress(state, address) {
            this.direccion = state.user.direcciones.find(direccion => direccion.id == address.id)
            this.direccion = address
        },
        deleteAddress(state, id) {
            state.user.direcciones = state.user.direcciones.filter(direccion => direccion.id !== id)
        },
        updateProductoToCarrito(state, prodCart) {
            const index = state.carrito.findIndex(item => item.id == prodCart.id);
            state.carrito.splice(index, 1, prodCart)
        },
        deleteProductoToCarrito(state, id) {
            const index = state.carrito.findIndex(item => item.id == id);
            state.carrito.splice(index, 1);
        },
        clearCarrito(state) {
            state.carrito = [];
        },
        setProductos(state, productos) {
            state.productos = productos
        },
        setProductsLoaded(state, loaded) {
            state.productsLoaded = loaded
        },
        setCategorias(state, categorias) {
            state.categorias = categorias;
        },
        retrieveToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null
        },
        retrieveUser(state, user) {
            state.user = user;
        }
    },
    actions: {
        loadData(context) {
            context.dispatch('getProductos');
            context.dispatch('getCategorias');
            if (context.getters.loggedIn) {
                context.dispatch('retrieveUser');
            }
            context.commit('setProductsLoaded', true)
        },
        retrieveUser(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            return new Promise((resolve, reject) => {
                axios.get('/user')
                    .then(response => {
                        context.commit('retrieveUser', response.data.data)
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        clearCarrito(context) {
            context.commit('clearCarrito')
        },
        addProductoToCarrito(context, prodCart) {
            context.commit('addProductoToCarrito', prodCart)
        },
        updateProductoToCarrito(context, prodCart) {
            context.commit('updateProductoToCarrito', prodCart)
        },
        deleteProductoToCarrito(context, id) {
            context.commit('deleteProductoToCarrito', id)
        },
        register(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('/register', {
                        name: data.name,
                        email: data.email,
                        password: data.password,
                        password_confirmation: data.password_confirmation,
                    })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        destroyToken(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.get('/logout')
                        .then(response => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(error => {
                            localStorage.removeItem('access_token')
                            context.commit('destroyToken')
                            reject(error)
                        })
                })
            }
        },
        retrieveToken(context, credentials) {

            return new Promise((resolve, reject) => {
                axios.post('/login', {
                        email: credentials.username,
                        password: credentials.password,
                    })
                    .then(response => {
                        const token = response.data.access_token

                        localStorage.setItem('access_token', token)
                        context.commit('retrieveToken', token)
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        reject(error)
                    })
            })
        },
        getProductos(context) {
            axios.get('/productos')
                .then(response => {
                    context.commit('setProductos', response.data.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        getCategorias(context) {
            axios.get('/categorias')
                .then(response => {
                    context.commit('setCategorias', response.data.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        addPedido(context, pedido) {
            axios.post('/pedidos', {
                    "id_cliente": pedido.cliente,
                    "id_repartidor": 0,
                    "id_direccion": pedido.address,
                    "lineas": this.getters.carrito
                })
                .then(response => {
                    context.commit('addPedido', response.data.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        updatePedido(context, pedido) {
            axios.patch('/pedidos/' + pedido.id, pedido)
                .then(response => {
                    context.commit('updatePedido', response.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        cambioEstado(context, data) {
            axios.put('/cambio-estado/' + data.id, {
                    'id_estado': data.estado
                })
                .then(response => {
                    context.commit('updateEstado', response.data.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        addAddress(context, data) {
            axios.post('/direcciones', {
                    'id_user': data.id_user,
                    'name': data.address.name,
                    'direccion_1': data.address.direccion_1,
                    'direccion_2': data.address.direccion_2,
                    'ciudad': data.address.ciudad,
                    'provincia': data.address.provincia,
                    'pais': data.address.pais,
                    'telefono': data.address.telefono
                })
                .then(response => {
                    context.commit('addAddress', response.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        updateAddress(context, data) {
            axios.put('/direcciones/' + data.id, {
                    'id_user': data.id_user,
                    'name': data.address.name,
                    'direccion_1': data.address.direccion_1,
                    'direccion_2': data.address.direccion_2,
                    'ciudad': data.address.ciudad,
                    'provincia': data.address.provincia,
                    'pais': data.address.pais,
                    'telefono': data.address.telefono
                })
                .then(response => {
                    context.commit('updateAddress', response.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
        deleteAddress(context, id) {
            axios.delete('/direcciones/' + id)
                .then(response => {
                    context.commit('deleteAddress', response.data)
                })
                .catch(error => {
                    console.log(error)
                })
        },
    }
})