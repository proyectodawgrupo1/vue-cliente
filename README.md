# batoilogic-cliente
## Descripcion
	Se trata de la aplicacion web para uso del cliente.
	El cliente puede ver los procutos, comprar y registrase,
	para realizar una compra primero tiene que esta loggeado.
	
## Dependencias
    axios: ^0.19.2,
    bootstrap-vue: ^2.5.0,
    core-js: ^3.4.4,
    router: ^1.3.4,
    vue: ^2.6.10,
    vue-router: ^3.1.5,
    vuex: "^3.1.2"

## Project setup
	npm install
	change in src/store/index.js the api url
	npm run build
## Wiki

[enlace](https://bitbucket.org/proyectodawgrupo1/vue-cliente/wiki/Home)

## Integrantes

